/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turmainterface;

import java.net.URL;
import java.util.ResourceBundle;
//import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import java.text.DecimalFormat;
/**
 *
 * @author augus
 */
public class FXMLDocumentController implements Initializable {
    Aluno[] aluno = new Aluno[100];
    float[] peso = new float[3];
    int cont=0;
    @FXML
    private Button cadastra;
     @FXML
    private Button quapeso;
    @FXML
    private TextField nome;
    @FXML
    private TextField n1;
    @FXML
    private TextField n2;
    @FXML
    private TextField n3;
    @FXML
    private TextField p1;
    @FXML
    private TextField p2;
    @FXML
    private TextField p3;
    @FXML
    private TextField ava1;
    @FXML
    private TextField ava2;
    @FXML
    private TextField ava3;
    @FXML
    private TextField aluesc;
    @FXML
    private Label alunos;
    @FXML
    private TextField aluhisto;
    @FXML
    private void cadastra() {
    aluno[cont] = new Aluno();
    aluno[cont].nome=nome.getText();
    aluno[cont].hist[0]=Double.parseDouble(n1.getText());
    aluno[cont].hist[1]=Double.parseDouble(n2.getText());
    aluno[cont].hist[2]=Double.parseDouble(n3.getText());
    System.out.println("Aluno cadastrado com sucesso");
    alunos.setText(alunos.getText()+"\n"+cont+"-"+aluno[cont].nome);  
    cont++;
    }
    @FXML
    private void pesos() {
    peso[0]=Float.parseFloat(p1.getText());
    peso[1]=Float.parseFloat(p2.getText());
    peso[2]=Float.parseFloat(p3.getText());
    System.out.println("Peso cadastrado com sucesso");
    }
    @FXML
    private void calcular(){
        double nota= 0.0;
        aluno[Integer.parseInt(aluesc.getText())].notastri[0]=Double.parseDouble(ava1.getText());
        if(aluno[Integer.parseInt(aluesc.getText())].notastri[0]==0){
                 aluno[Integer.parseInt(aluesc.getText())].notastri[0] = 0.1;
                 }
        aluno[Integer.parseInt(aluesc.getText())].notastri[1]=Double.parseDouble(ava2.getText());
        if(aluno[Integer.parseInt(aluesc.getText())].notastri[1]==0){
                 aluno[Integer.parseInt(aluesc.getText())].notastri[1] = 0.1;
                 }
        aluno[Integer.parseInt(aluesc.getText())].notastri[2]=Double.parseDouble(ava3.getText());
        if(aluno[Integer.parseInt(aluesc.getText())].notastri[2]==0){
                 aluno[Integer.parseInt(aluesc.getText())].notastri[2] = 0.1;
                 }
        nota+= peso[0] / aluno[Integer.parseInt(aluesc.getText())].notastri[0];
        nota+= peso[1] / aluno[Integer.parseInt(aluesc.getText())].notastri[1];
        nota+= peso[2] / aluno[Integer.parseInt(aluesc.getText())].notastri[2];
        aluno[Integer.parseInt(aluesc.getText())].media=10/nota;
        System.out.printf("Nota final: %.2f\n",aluno[Integer.parseInt(aluesc.getText())].media);
        System.out.println("Nota registrada com sucesso");
    }
    @FXML
    private void mostrahist(){
            System.out.println("Histórico:");
            DecimalFormat formata = new DecimalFormat();
            System.out.println("Escolha um aluno:");
            for(int cont1=0;cont1<cont;cont1++){
                System.out.println(cont1 + "-" + aluno[cont1].nome);
            }
            int aluhist = Integer.parseInt(aluhisto.getText());
            System.out.println("Aluno: " + aluno[aluhist].nome);
            System.out.println("Nota do trimestre atual\t\tNotas Programação 1\n" + formata.format(aluno[aluhist].media) + "\t\t\t\t1ºTri: " + aluno[aluhist].hist[0]+" | 2ºTri: "+ aluno[aluhist].hist[1]+" | 3ºTri: "+ aluno[aluhist].hist[2]);
    }
    @FXML
    void mostranota(){
       System.out.println("Notas atuais:");
       System.out.printf("Nome:\t");
       for(int cont1=0;cont1<3;cont1++){
                System.out.printf("%dº Avaliação\t",++cont1);
                cont1--;
       }
       System.out.println("\n");
       for(int cont1=0;cont1<cont;cont1++){
            System.out.printf("%s\t\t",aluno[cont1].nome);
            for(int cont2=0;cont2<3;cont2++){
                System.out.printf("%.2f\t\t",aluno[cont1].notastri[cont2]);
            }
            System.out.println("\n");
       }
   }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
